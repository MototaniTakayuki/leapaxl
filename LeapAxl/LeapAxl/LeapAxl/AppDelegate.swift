//
//  AppDelegate.swift
//  LeapAxl
//
//  Created by t-mototani on 2017/06/24.
//  Copyright © 2017年 t-mototani. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

