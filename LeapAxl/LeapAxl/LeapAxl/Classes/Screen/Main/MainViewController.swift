//
//  MainViewController.swift
//  LeapAxl
//
//  Created by t-mototani on 2017/06/24.
//  Copyright © 2017年 t-mototani. All rights reserved.
//

import Cocoa

class MainViewController: BaseViewController {
    
    private var images: Array<String>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let windowController = self.windowController() as! LeapAxlWindowController
            windowController.contentViewController = StoryboardUtil.fileViewController()
        }
    }
}
