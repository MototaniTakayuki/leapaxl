//
//  FileViewController.swift
//  LeapAxl
//
//  Created by t-mototani on 2017/06/25.
//  Copyright © 2017年 t-mototani. All rights reserved.
//

import Cocoa

class FileViewController: BaseViewController, NSTableViewDataSource, NSTableViewDelegate {

    
    private var fileList: Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onTappedButtonOpen(_ sender: Any) {
        let openPanel :NSOpenPanel = NSOpenPanel()
        openPanel.canChooseFiles = false             // ファイル選択を許可する
        openPanel.canChooseDirectories = true      // フォルダ選択は禁止する
        openPanel.allowsMultipleSelection = false    // 複数選択を許可する
        openPanel.message = "Open"                 // ダイアログに表記される説明
        openPanel.begin(completionHandler: { (num) -> Void in
            if num == NSModalResponseOK {
                for fileURL in openPanel.urls {
                    let filePath = fileURL.path
                    print(fileURL.path)
                    self.validateShow(filePath: filePath)
                }
            } else if num == NSModalResponseCancel {
                print("Canceled")
            }
        })
    }
    
    private func validateShow(filePath: String) {
        do {
            let list = try FileManager().contentsOfDirectory(atPath: filePath)
            if list.count == 0 {
                return
            }
            ////////// Sort /////////////
            let itemName = list[0]
            let string = NSString(string: list[0])
            let ex = string.pathExtension
            let numbers = string.components(separatedBy: CharacterSet.decimalDigits.inverted)
            var num = ""
            for item in numbers {
                if (item != "") {
                    num = item
                }
            }
            let currentIndex = itemName.index(itemName.endIndex, offsetBy: -(num.characters.count + ex.characters.count + 1))
            var head = itemName.substring(to:currentIndex)
            var pathNums = Array<Int>()
            for path in list {
                let frist = path.index(path.startIndex, offsetBy: head.characters.count)
                let str = path.substring(from: frist)
                let second = str.index(str.endIndex, offsetBy: -(offsetBy: ex.characters.count + 1))
                let str2 = str.substring(to: second)
                
                pathNums.append(Int(str2)!)
            }
            pathNums.sort()
            ///////////// Sort /////////////
            for item in pathNums {
                self.fileList.append(filePath + "/" + head + String(item) + "." + ex)
            }
            let windowController = self.windowController() as! LeapAxlWindowController
            let viewController = StoryboardUtil.viewerViewController() as! ViewerViewController
            viewController.fileDirPathList = self.fileList
            windowController.contentViewController = viewController
        } catch {
            
        }
    }
}
