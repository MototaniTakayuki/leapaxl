//
//  BaseViewController.swift
//  LeapAxl
//
//  Created by t-mototani on 2017/06/25.
//  Copyright © 2017年 t-mototani. All rights reserved.
//

import Cocoa

class BaseViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func windowController() -> NSWindowController {
        return (self.view.window?.windowController)!
    }
    
}
