//
//  ViewerViewController.swift
//  LeapAxl
//
//  Created by t-mototani on 2017/06/25.
//  Copyright © 2017年 t-mototani. All rights reserved.
//

import Cocoa

class ViewerViewController: BaseViewController, LeapNotificaterDelegate {

    @IBOutlet weak var imageView: NSImageView!
    
    public var fileDirPathList: Array<String> = []
    private let leap = LeapNotificater();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leap.delegate = self;
        self.leap.run()
        if self.imageView.layer != nil {
            let color : CGColor = CGColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
            self.imageView.layer?.backgroundColor = color
        }
        displayImage(index: 0)
    }
    
    private func displayImage(index: Int) {
        if self.fileDirPathList.count - 1 < index {
            return
        }
        let image: NSImage = NSImage(contentsOfFile: self.fileDirPathList[index])!
        self.imageView.image = image
    }
    
    func leapNotificaterDelegate(_ delegate: LeapNotificater!, didPositionY y: Float) {
        let index = Int(y)
        displayImage(index: index)
    }
    
}
