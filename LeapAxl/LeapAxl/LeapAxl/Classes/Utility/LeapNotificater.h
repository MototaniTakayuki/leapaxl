//
//  LeapNSNotificater.h
//  LeapHand
//
//  Created by Mototani Takayuki on 2017/03/06.
//  Copyright © 2017年 Mototani Takayuki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LeapObjectiveC.h"

@protocol LeapNotificaterDelegate;

@interface LeapNotificater : NSObject<LeapListener>

@property (weak, nonatomic)id<LeapNotificaterDelegate>delegate;

- (void)run;
- (void)stop;

@property (nonatomic) LeapController *leapController;

@end

@protocol LeapNotificaterDelegate <NSObject>

@optional
- (void)leapNotificaterDelegate:(LeapNotificater *)delegate didGetFingerCount:(int)count;
- (void)leapNotificaterDelegate:(LeapNotificater *)delegate didGetFrame:(LeapController *)controller;
- (void)leapNotificaterDelegate:(LeapNotificater *)delegate didPositionY:(float)y;

@end
