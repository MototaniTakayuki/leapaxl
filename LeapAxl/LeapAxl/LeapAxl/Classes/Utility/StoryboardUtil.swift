//
//  StoryboardUtil.swift
//  LeapAxl
//
//  Created by t-mototani on 2017/06/25.
//  Copyright © 2017年 t-mototani. All rights reserved.
//

import Cocoa

class StoryboardUtil: NSObject {

    public static func fileViewController() -> NSViewController {
        return StoryboardUtil.storyboardWithName(boardName: "File", VCname: "FileViewController")
    }
    
    public static func viewerViewController() -> NSViewController {
        return StoryboardUtil.storyboardWithName(boardName: "Viewer", VCname: "ViewerViewController")
    }
    
    public static func mainViewController() -> NSViewController {
        return StoryboardUtil.storyboardWithName(boardName: "Main", VCname: "MainViewController")
    }
    
    public static func storyboardWithName(boardName: String, VCname: String) -> NSViewController {
        let board: NSStoryboard = NSStoryboard(name: boardName, bundle: nil)
        let viewController: NSViewController = board.instantiateController(withIdentifier: VCname) as! NSViewController
        return viewController
    }
}
