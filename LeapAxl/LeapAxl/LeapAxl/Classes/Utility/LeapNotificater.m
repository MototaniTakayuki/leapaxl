//
//  LeapNSNotificater.m
//  LeapHand
//
//  Created by Mototani Takayuki on 2017/03/06.
//  Copyright © 2017年 Mototani Takayuki. All rights reserved.
//

#import "LeapNotificater.h"

@interface LeapNotificater ()

@property(nonatomic) NSArray *fingerNames;
@property(nonatomic) NSArray *boneNames;

@end

@implementation LeapNotificater

- (id)init {
    self = [super init];
    static const NSString *const fingerNamesInit[] = {
        @"Thumb", @"Index finger", @"Middle finger",
        @"Ring finger", @"Pinky"
    };
    static const NSString *const boneNamesInit[] = {
        @"Metacarpal", @"Proximal phalanx",
        @"Intermediate phalanx", @"Distal phalanx"
    };
    self.fingerNames = [[NSArray alloc] initWithObjects:fingerNamesInit count:5];
    self.boneNames = [[NSArray alloc] initWithObjects:boneNamesInit count:4];
    self.leapController = [[LeapController alloc] init];
    return self;
}

- (void)run {
    [self.leapController addListener:self];
    [self.leapController setPolicy:LEAP_POLICY_IMAGES];
    [self.leapController.config save];
    NSLog(@"RUN!");
}

- (void)stop {
    [self.leapController removeListener:self];
    NSLog(@"STOP");
}

#pragma mark - Callbacks

- (void)onInit:(NSNotification *)notification {
    NSLog(@"Initialized");
}

- (void)onConnect:(NSNotification *)notification {
    NSLog(@"Connected");
    LeapController *controller = (LeapController *)[notification object];
    [controller enableGesture:LEAP_GESTURE_TYPE_CIRCLE enable:YES];
    [controller enableGesture:LEAP_GESTURE_TYPE_KEY_TAP enable:YES];
    [controller enableGesture:LEAP_GESTURE_TYPE_SCREEN_TAP enable:YES];
    [controller enableGesture:LEAP_GESTURE_TYPE_SWIPE enable:YES];
}

- (void)onDisconnect:(NSNotification *)notification {
    //Note: not dispatched when running in a debugger.
    NSLog(@"Disconnected");
}

- (void)onServiceConnect:(NSNotification *)notification {
    NSLog(@"Service Connected");
}

- (void)onServiceDisconnect:(NSNotification *)notification {
    NSLog(@"Service Disconnected");
}

- (void)onDeviceChange:(NSNotification *)notification {
    NSLog(@"Device Changed");
}

- (void)onExit:(NSNotification *)notification {
    NSLog(@"Exited");
}

- (void)onFrame:(NSNotification *)notification {
    LeapController *controller = (LeapController *)[notification object];
    LeapFrame *frame = [controller frame:0];
//    NSLog(@"Frame id: %lld, timestamp: %lld, hands: %ld, extended fingers: %ld, tools: %ld, gestures: %ld",
//          [frame id], [frame timestamp], [[frame hands] count],
//          [[[frame fingers] extended] count], [[frame tools] count], [[frame gestures:nil] count]);
    if ([self.delegate respondsToSelector:@selector(leapNotificaterDelegate:didGetFingerCount:)]) {
        [self.delegate leapNotificaterDelegate:self didGetFingerCount:(int)[[[frame fingers] extended] count]];
    }
    
    if ([self.delegate respondsToSelector:@selector(leapNotificaterDelegate:didGetFrame:)]) {
        [self.delegate leapNotificaterDelegate:self didGetFrame:controller];
    }
    if ([self.delegate respondsToSelector:@selector(leapNotificaterDelegate:didPositionY:)]) {
        if (frame.hands.count != 0) {
            NSLog(@"tip position %f", [frame.hands[0] palmPosition].y);
            [self.delegate leapNotificaterDelegate:self didPositionY:[frame.hands[0] palmPosition].y];
        }
    }
    
//    // Get hands
//    for (LeapHand *hand in frame.hands) {
//        NSString *handType = hand.isLeft ? @"Left hand" : @"Right hand";
//        NSLog(@"  %@, id: %i, palm position: %@",
//              handType, hand.id, hand.palmPosition);
        
        
        
//        // Get the hand's normal vector and direction
//        const LeapVector *normal = [hand palmNormal];
//        const LeapVector *direction = [hand direction];
//        
//        // Calculate the hand's pitch, roll, and yaw angles
//        NSLog(@"  pitch: %f degrees, roll: %f degrees, yaw: %f degrees\n",
//              [direction pitch] * LEAP_RAD_TO_DEG,
//              [normal roll] * LEAP_RAD_TO_DEG,
//              [direction yaw] * LEAP_RAD_TO_DEG);
//        
//        // Get the Arm bone
//        LeapArm *arm = hand.arm;
//        NSLog(@"    Arm direction: %@, wrist position: %@, elbow position: %@", arm.direction, arm.wristPosition, arm.elbowPosition);
//        
//        for (LeapFinger *finger in hand.fingers) {
//            NSLog(@"    %@, id: %i, length: %fmm, width: %fmm",
//                  [self.fingerNames objectAtIndex:finger.type],
//                  finger.id, finger.length, finger.width);
//            
//            for (int boneType = LEAP_BONE_TYPE_METACARPAL; boneType <= LEAP_BONE_TYPE_DISTAL; boneType++) {
//                LeapBone *bone = [finger bone:boneType];
//                NSLog(@"      %@ bone, start: %@, end: %@, direction: %@",
//                      [self.boneNames objectAtIndex:boneType], bone.prevJoint, bone.nextJoint, bone.direction);
//            }
//        }
//    }
    
    for (LeapTool *tool in frame.tools) {
        NSLog(@"  Tool, id: %i, position: %@, direction: %@", tool.id, tool.tipPosition, tool.direction);
    }
    
    NSArray *gestures = [frame gestures:nil];
    for (int g = 0; g < [gestures count]; g++) {
        LeapGesture *gesture = [gestures objectAtIndex:g];
        switch (gesture.type) {
            case LEAP_GESTURE_TYPE_CIRCLE: {
                LeapCircleGesture *circleGesture = (LeapCircleGesture *)gesture;
                
                NSString *clockwiseness;
                if ([[[circleGesture pointable] direction] angleTo:[circleGesture normal]] <= LEAP_PI/2) {
                    clockwiseness = @"clockwise";
                } else {
                    clockwiseness = @"counterclockwise";
                }
                
                // Calculate the angle swept since the last frame
                float sweptAngle = 0;
                if(circleGesture.state != LEAP_GESTURE_STATE_START) {
                    LeapCircleGesture *previousUpdate = (LeapCircleGesture *)[[controller frame:1] gesture:gesture.id];
                    sweptAngle = (circleGesture.progress - previousUpdate.progress) * 2 * LEAP_PI;
                }
                
                NSLog(@"  Circle id: %d, %@, progress: %f, radius %f, angle: %f degrees %@",
                      circleGesture.id, [LeapNotificater stringForState:gesture.state],
                      circleGesture.progress, circleGesture.radius,
                      sweptAngle * LEAP_RAD_TO_DEG, clockwiseness);
                break;
            }
            case LEAP_GESTURE_TYPE_SWIPE: {
                LeapSwipeGesture *swipeGesture = (LeapSwipeGesture *)gesture;
                NSLog(@"  Swipe id: %d, %@, position: %@, direction: %@, speed: %f",
                      swipeGesture.id, [LeapNotificater stringForState:swipeGesture.state],
                      swipeGesture.position, swipeGesture.direction, swipeGesture.speed);
                break;
            }
            case LEAP_GESTURE_TYPE_KEY_TAP: {
                LeapKeyTapGesture *keyTapGesture = (LeapKeyTapGesture *)gesture;
                NSLog(@"  Key Tap id: %d, %@, position: %@, direction: %@",
                      keyTapGesture.id, [LeapNotificater stringForState:keyTapGesture.state],
                      keyTapGesture.position, keyTapGesture.direction);
                break;
            }
            case LEAP_GESTURE_TYPE_SCREEN_TAP: {
                LeapScreenTapGesture *screenTapGesture = (LeapScreenTapGesture *)gesture;
                NSLog(@"  Screen Tap id: %d, %@, position: %@, direction: %@",
                      screenTapGesture.id, [LeapNotificater stringForState:screenTapGesture.state],
                      screenTapGesture.position, screenTapGesture.direction);
                break;
            }
            default:
                NSLog(@"  Unknown gesture type");
                break;
        }
    }
    
    if (([[frame hands] count] > 0) || [[frame gestures:nil] count] > 0) {
        NSLog(@" ");
    }
}

- (void)onFocusGained:(NSNotification *)notification {
    NSLog(@"Focus Gained");
}

- (void)onFocusLost:(NSNotification *)notification {
    NSLog(@"Focus Lost");
}

+ (NSString *)stringForState:(LeapGestureState)state {
    switch (state) {
        case LEAP_GESTURE_STATE_INVALID:
            return @"STATE_INVALID";
        case LEAP_GESTURE_STATE_START:
            return @"STATE_START";
        case LEAP_GESTURE_STATE_UPDATE:
            return @"STATE_UPDATED";
        case LEAP_GESTURE_STATE_STOP:
            return @"STATE_STOP";
        default:
            return @"STATE_INVALID";
    }
}

@end
